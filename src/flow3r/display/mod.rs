use core::convert::Infallible;

use ::display_interface::DisplayError;
use embassy_time::{Duration, Timer};
use embedded_graphics::{
    pixelcolor::Rgb565,
    prelude::{Dimensions, DrawTarget, OriginDimensions, RgbColor, Size},
    primitives::{PrimitiveStyle, Rectangle, StyledDrawable},
};
use embedded_graphics_framebuf::FrameBuf;
use esp_println::println;
use hal::{
    gdma::ChannelCreator0,
    gpio::{Gpio38, Gpio40, Gpio46, Unknown},
    ledc::LEDC,
    peripherals::SPI2,
    prelude::_esp_hal_ledc_channel_ChannelIFace,
    spi::FullDuplexMode,
    Spi,
};
use static_cell::StaticCell;

use self::{backlight::init_backlight, framebuffer::DmaFrameBuffer};

use self::{
    backlight::Backlight,
    display_driver::{init_display_driver, DisplayDriver},
};

mod backlight;
mod display_driver;
mod display_interface;
mod framebuffer;

static DMA_DESCRIPTORS_DISPLAY: StaticCell<[u32; 27]> = StaticCell::new();
static DMA_DESCRIPTORS_RX_DISPLAY: StaticCell<[u32; 27]> = StaticCell::new();

pub struct Display {
    driver: DisplayDriver<'static>,
    framebuffer: FrameBuf<Rgb565, DmaFrameBuffer>,
    backlight: Backlight,
}

impl Display {
    pub async fn new<'a>(
        spi: Spi<'static, SPI2, FullDuplexMode>,
        channel: ChannelCreator0,
        ledc: LEDC<'static>,
        bl: Gpio46<Unknown>,
        dc: Gpio38<Unknown>,
        cs: Gpio40<Unknown>,
    ) -> Self {
        let descriptors = DMA_DESCRIPTORS_DISPLAY.init([0u32; 27]);
        let rx_descriptors = DMA_DESCRIPTORS_RX_DISPLAY.init([0u32; 27]);
        Self {
            driver: init_display_driver(spi, channel, descriptors, rx_descriptors, dc, cs).await,
            framebuffer: FrameBuf::new(DmaFrameBuffer::take(), 240, 240),
            backlight: init_backlight(ledc, bl),
        }
    }

    pub async fn clear(&mut self) -> Result<(), DisplayError> {
        Rectangle::with_center(
            self.framebuffer.bounding_box().center(),
            Size::new(240, 240),
        )
        .draw_styled(
            &PrimitiveStyle::with_fill(Rgb565::BLACK),
            &mut self.framebuffer,
        )
        .unwrap();
        self.framebuffer.data.draw(&mut self.driver).await
    }

    pub fn set_backlight(&self, percent: u8) -> Result<(), hal::ledc::channel::Error> {
        self.backlight.set_duty(percent)
    }

    pub async fn flush(&mut self) -> Result<(), DisplayError> {
        self.framebuffer.data.draw(&mut self.driver).await
    }
}

impl OriginDimensions for Display {
    fn size(&self) -> Size {
        self.framebuffer.size()
    }
}

impl DrawTarget for Display {
    type Color = Rgb565;

    type Error = Infallible;

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = embedded_graphics::Pixel<Self::Color>>,
    {
        self.framebuffer.draw_iter(pixels)
    }
}

#[embassy_executor::task]
pub async fn display_refresh() {
    loop {
        println!("running on second core");
        Timer::after(Duration::from_secs(1)).await;
    }
}
