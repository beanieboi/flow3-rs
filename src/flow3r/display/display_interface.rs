use display_interface::{
    AsyncDmaWriteOnlyDataCommand, AsyncWriteOnlyDataCommand, DataFormat, DisplayError,
};
use embedded_hal::blocking::spi::Write;
use embedded_hal::digital::v2::OutputPin;
use hal::{
    dma::{ChannelTypes, SpiPeripheral},
    prelude::*,
    spi::{dma::SpiDma, InstanceDma, IsFullDuplex},
};

pub struct SpiDmaInterface<SPI, DC, CS> {
    spi: Option<SPI>,
    dc: DC,
    cs: CS,
}

impl<'d, T, C, M, DC, CS> SpiDmaInterface<SpiDma<'d, T, C, M>, DC, CS>
where
    T: InstanceDma<<C as ChannelTypes>::Tx<'d>, <C as ChannelTypes>::Rx<'d>>,
    C: ChannelTypes,
    <C as ChannelTypes>::P: SpiPeripheral,
    M: IsFullDuplex,
    DC: OutputPin,
    CS: OutputPin,
{
    /// Create new SPI interface for communciation with a display driver
    pub fn new(spi: SpiDma<'d, T, C, M>, dc: DC, cs: CS) -> Self {
        Self {
            spi: Some(spi),
            dc,
            cs,
        }
    }

    /// Consume the display interface and return
    /// the underlying peripherial driver and GPIO pins used by it
    pub fn _release(self) -> (SpiDma<'d, T, C, M>, DC, CS) {
        (self.spi.unwrap(), self.dc, self.cs)
    }
}

impl<'d, T, C, M, DC, CS> AsyncDmaWriteOnlyDataCommand
    for SpiDmaInterface<SpiDma<'d, T, C, M>, DC, CS>
where
    T: InstanceDma<<C as ChannelTypes>::Tx<'d>, <C as ChannelTypes>::Rx<'d>>,
    C: ChannelTypes,
    <C as ChannelTypes>::P: SpiPeripheral,
    M: IsFullDuplex,
    DC: OutputPin,
    CS: OutputPin,
{
    async fn send_commands_dma<D: embedded_dma::ReadBuffer<Word = u8>>(
        &mut self,
        cmds: D,
    ) -> Result<D, DisplayError> {
        self.cs.set_low().map_err(|_| DisplayError::CSError)?;
        // 1 = data, 0 = command
        self.dc.set_low().map_err(|_| DisplayError::DCError)?;

        // Send words over SPI
        let spi = self.spi.take().unwrap();
        let transfer = spi
            .dma_write(cmds)
            .map_err(|_| DisplayError::BusWriteError)?;
        let (cmds, spi) = transfer.wait().ok().unwrap(); // TODO fix
        let _ = self.spi.insert(spi);

        self.cs.set_high().ok();

        Ok(cmds)
    }

    async fn send_data_dma<D: embedded_dma::ReadBuffer<Word = u8>>(
        &mut self,
        buf: D,
    ) -> Result<D, DisplayError> {
        self.cs.set_low().map_err(|_| DisplayError::CSError)?;
        // 1 = data, 0 = command
        self.dc.set_high().map_err(|_| DisplayError::DCError)?;

        let spi = self.spi.take().unwrap();
        let transfer = spi
            .dma_write(buf)
            .map_err(|_| DisplayError::BusWriteError)?;

        /*let mut i = 0;
        while !transfer.is_done() && i < 10 {
            Timer::after(Duration::from_millis(1)).await;
            i += 1;
        }*/

        let (buf, spi) = transfer.wait().ok().unwrap();
        let _ = self.spi.insert(spi);

        self.cs.set_high().ok();

        Ok(buf)
    }
}

// Make this actually async lol
impl<'d, T, C, M, DC, CS> AsyncWriteOnlyDataCommand for SpiDmaInterface<SpiDma<'d, T, C, M>, DC, CS>
where
    T: InstanceDma<<C as ChannelTypes>::Tx<'d>, <C as ChannelTypes>::Rx<'d>>,
    C: ChannelTypes,
    <C as ChannelTypes>::P: SpiPeripheral,
    M: IsFullDuplex,
    DC: OutputPin,
    CS: OutputPin,
{
    async fn send_commands(&mut self, cmds: DataFormat<'_>) -> Result<(), DisplayError> {
        self.cs.set_low().map_err(|_| DisplayError::CSError)?;
        // 1 = data, 0 = command
        self.dc.set_low().map_err(|_| DisplayError::DCError)?;

        // Send words over SPI
        let spi = self.spi.take().unwrap();
        let spi = send_u8(spi, cmds)?;
        let _ = self.spi.insert(spi);

        self.cs.set_high().ok();

        Ok(())
    }

    async fn send_data(&mut self, buf: DataFormat<'_>) -> Result<(), DisplayError> {
        self.cs.set_low().map_err(|_| DisplayError::CSError)?;

        // 1 = data, 0 = command
        self.dc.set_high().map_err(|_| DisplayError::DCError)?;

        // Send words over SPI
        let spi = self.spi.take().unwrap();
        let spi = send_u8(spi, buf)?;
        let _ = self.spi.insert(spi);

        self.cs.set_high().ok();

        Ok(())
    }
}

fn send_u8<SPI: Write<u8>>(mut spi: SPI, words: DataFormat<'_>) -> Result<SPI, DisplayError> {
    match words {
        DataFormat::U8(slice) => {
            spi.write(slice).map_err(|_| DisplayError::BusWriteError)?;
            Ok(spi)
        }
        DataFormat::U16(slice) => {
            use byte_slice_cast::*;
            spi.write(slice.as_byte_slice())
                .map_err(|_| DisplayError::BusWriteError)?;
            Ok(spi)
        }
        DataFormat::U16LE(slice) => {
            use byte_slice_cast::*;
            for v in slice.as_mut() {
                *v = v.to_le();
            }
            spi.write(slice.as_byte_slice())
                .map_err(|_| DisplayError::BusWriteError)?;
            Ok(spi)
        }
        DataFormat::U16BE(slice) => {
            use byte_slice_cast::*;
            for v in slice.as_mut() {
                *v = v.to_be();
            }
            spi.write(slice.as_byte_slice())
                .map_err(|_| DisplayError::BusWriteError)?;
            Ok(spi)
        }
        DataFormat::U8Iter(iter) => {
            let mut buf = [0; 32];
            let mut i = 0;

            for v in iter.into_iter() {
                buf[i] = v;
                i += 1;

                if i == buf.len() {
                    spi.write(&buf).map_err(|_| DisplayError::BusWriteError)?;
                    i = 0;
                }
            }

            if i > 0 {
                spi.write(&buf[..i])
                    .map_err(|_| DisplayError::BusWriteError)?;
            }

            Ok(spi)
        }
        DataFormat::U16LEIter(iter) => {
            use byte_slice_cast::*;
            let mut buf = [0; 32];
            let mut i = 0;

            for v in iter.map(u16::to_le) {
                buf[i] = v;
                i += 1;

                if i == buf.len() {
                    spi.write(&buf.as_byte_slice())
                        .map_err(|_| DisplayError::BusWriteError)?;
                    i = 0;
                }
            }

            if i > 0 {
                spi.write(&buf[..i].as_byte_slice())
                    .map_err(|_| DisplayError::BusWriteError)?;
            }

            Ok(spi)
        }
        DataFormat::U16BEIter(iter) => {
            use byte_slice_cast::*;
            let mut buf = [0; 64];
            let mut i = 0;
            let len = buf.len();

            for v in iter.map(u16::to_be) {
                buf[i] = v;
                i += 1;

                if i == len {
                    spi.write(&buf.as_byte_slice())
                        .map_err(|_| DisplayError::BusWriteError)?;
                    i = 0;
                }
            }

            if i > 0 {
                spi.write(&buf[..i].as_byte_slice())
                    .map_err(|_| DisplayError::BusWriteError)?;
            }

            Ok(spi)
        }
        _ => Err(DisplayError::DataFormatNotImplemented),
    }
}
