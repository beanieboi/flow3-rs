use esp_hal_smartled::{smartLedAdapter, SmartLedsAdapter};
use hal::{
    gpio::{Gpio14, Unknown}, rmt::Channel0, Rmt,
};

pub type Leds = SmartLedsAdapter<Channel0<0>, 0, 961>;

pub fn init_leds(rmt: Rmt, pin: Gpio14<Unknown>) -> Leds {
    <smartLedAdapter!(0, 40)>::new(rmt.channel0, pin)
}

pub fn brightness_fade_in_out(brightness: (u8, bool), max: u8, min: u8) -> (u8, bool) {
    match brightness {
        (v, true) if v < max => (v + 1, true),
        (v, true) => (v - 1, false),
        (v, false) if v > min => (v - 1, false),
        (v, false) => (v + 1, true),
    }
}
