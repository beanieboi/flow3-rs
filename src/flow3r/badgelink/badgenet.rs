use embassy_executor::Spawner;
use embassy_net::{Config, Ipv6Address, Ipv6Cidr, Stack, StackResources, StaticConfigV6};
use embassy_net_badgelink::{Device, Runner, State};
use esp_println::println;
use hal::{
    efuse::Efuse,
    peripherals::{UART0, UART1, UART2},
    Rng, Uart,
};
use heapless::Vec;
use static_cell::StaticCell;

static STATE_LEFT: StaticCell<State<8, 8>> = StaticCell::new();
static STACK_LEFT: StaticCell<Stack<Device<'static>>> = StaticCell::new();
static STACK_RESOURCES_LEFT: StaticCell<StackResources<2>> = StaticCell::new();
static STATE_RIGHT: StaticCell<State<8, 8>> = StaticCell::new();
static STACK_RIGHT: StaticCell<Stack<Device<'static>>> = StaticCell::new();
static STACK_RESOURCES_RIGHT: StaticCell<StackResources<2>> = StaticCell::new();

pub type BadgenetUartLeft = Uart<'static, UART1>;
pub type BadgenetUartRight = Uart<'static, UART2>;

pub async fn start_badgenet_left(uart: BadgenetUartLeft, seed: u64, config: Config ) -> &'static Stack<Device<'static>> {
    let mac_addr = Efuse::get_mac_address();
    let state = STATE_LEFT.init(State::<8, 8>::new());
    let (device, runner) = embassy_net_badgelink::new(mac_addr, state, uart);
    let stack = STACK_LEFT.init(Stack::new(
        device,
        config,
        STACK_RESOURCES_LEFT.init(StackResources::new()),
        seed,
    ));

    let spawner = Spawner::for_current_executor().await;
    spawner.spawn(runner_0_task(runner)).ok();
    spawner.spawn(stack_task_0(stack)).ok();

    println!("started left badgenet");

    stack
}

pub async fn start_badgenet_right(uart: BadgenetUartRight, seed: u64, config: Config) -> &'static Stack<Device<'static>> {
    let mac_addr = Efuse::get_mac_address();
    let state = STATE_RIGHT.init(State::<8, 8>::new());
    let (device, runner) = embassy_net_badgelink::new(mac_addr, state, uart);
    let stack = STACK_RIGHT.init(Stack::new(
        device,
        config,
        STACK_RESOURCES_RIGHT.init(StackResources::new()),
        seed,
    ));

    let spawner = Spawner::for_current_executor().await;
    spawner.spawn(runner_1_task(runner)).ok();
    spawner.spawn(stack_task_1(stack)).ok();

    println!("started right badgenet");

    stack
}

#[embassy_executor::task]
pub async fn runner_0_task(runner: Runner<'static, BadgenetUartLeft>) -> ! {
    println!("left badgenet runner started");
    runner.run().await
}

#[embassy_executor::task]
pub async fn runner_1_task(runner: Runner<'static, BadgenetUartRight>) -> ! {
    println!("right badgenet runner started");
    runner.run().await
}

#[embassy_executor::task]
pub async fn stack_task_0(stack: &'static Stack<Device<'static>>) -> ! {
    println!("left badgenet stack started");
    stack.run().await
}

#[embassy_executor::task]
pub async fn stack_task_1(stack: &'static Stack<Device<'static>>) -> ! {
    println!("right badgenet stack started");
    stack.run().await
}
