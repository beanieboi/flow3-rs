
#[derive(Debug, Clone, Copy)]
pub struct InterruptStatus {
    pub stage_low: StageLowInterruptStatus,
    pub stage_high: StageHighInterruptStatus,
    pub stage_complete: StageCompleteInterruptStatus,
}

impl InterruptStatus {
    pub(crate) fn from_registers(regs: [u16; 3]) -> Self {
        Self {
            stage_low: StageLowInterruptStatus::from_register(regs[0]),
            stage_high: StageHighInterruptStatus::from_register(regs[1]),
            stage_complete: StageCompleteInterruptStatus::from_register(regs[2]),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct StageLowInterruptStatus {
    pub stage0: bool,
    pub stage1: bool,
    pub stage2: bool,
    pub stage3: bool,
    pub stage4: bool,
    pub stage5: bool,
    pub stage6: bool,
    pub stage7: bool,
    pub stage8: bool,
    pub stage9: bool,
    pub stage10: bool,
    pub stage11: bool,
}

impl StageLowInterruptStatus {
    pub(crate) fn from_register(reg: u16) -> Self {
        Self {
            stage0: ((reg >> 0) & 0x01) != 0,
            stage1: ((reg >> 1) & 0x01) != 0,
            stage2: ((reg >> 2) & 0x01) != 0,
            stage3: ((reg >> 3) & 0x01) != 0,
            stage4: ((reg >> 4) & 0x01) != 0,
            stage5: ((reg >> 5) & 0x01) != 0,
            stage6: ((reg >> 6) & 0x01) != 0,
            stage7: ((reg >> 7) & 0x01) != 0,
            stage8: ((reg >> 8) & 0x01) != 0,
            stage9: ((reg >> 9) & 0x01) != 0,
            stage10: ((reg >> 10) & 0x01) != 0,
            stage11: ((reg >> 11) & 0x01) != 0,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct StageHighInterruptStatus {
    pub stage0: bool,
    pub stage1: bool,
    pub stage2: bool,
    pub stage3: bool,
    pub stage4: bool,
    pub stage5: bool,
    pub stage6: bool,
    pub stage7: bool,
    pub stage8: bool,
    pub stage9: bool,
    pub stage10: bool,
    pub stage11: bool,
}

impl StageHighInterruptStatus {
    pub(crate) fn from_register(reg: u16) -> Self {
        Self {
            stage0: ((reg >> 0) & 0x01) != 0,
            stage1: ((reg >> 1) & 0x01) != 0,
            stage2: ((reg >> 2) & 0x01) != 0,
            stage3: ((reg >> 3) & 0x01) != 0,
            stage4: ((reg >> 4) & 0x01) != 0,
            stage5: ((reg >> 5) & 0x01) != 0,
            stage6: ((reg >> 6) & 0x01) != 0,
            stage7: ((reg >> 7) & 0x01) != 0,
            stage8: ((reg >> 8) & 0x01) != 0,
            stage9: ((reg >> 9) & 0x01) != 0,
            stage10: ((reg >> 10) & 0x01) != 0,
            stage11: ((reg >> 11) & 0x01) != 0,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct StageCompleteInterruptStatus {
    pub stage0: bool,
    pub stage1: bool,
    pub stage2: bool,
    pub stage3: bool,
    pub stage4: bool,
    pub stage5: bool,
    pub stage6: bool,
    pub stage7: bool,
    pub stage8: bool,
    pub stage9: bool,
    pub stage10: bool,
    pub stage11: bool,
}

impl StageCompleteInterruptStatus {
    pub(crate) fn from_register(reg: u16) -> Self {
        Self {
            stage0: ((reg >> 0) & 0x01) != 0,
            stage1: ((reg >> 1) & 0x01) != 0,
            stage2: ((reg >> 2) & 0x01) != 0,
            stage3: ((reg >> 3) & 0x01) != 0,
            stage4: ((reg >> 4) & 0x01) != 0,
            stage5: ((reg >> 5) & 0x01) != 0,
            stage6: ((reg >> 6) & 0x01) != 0,
            stage7: ((reg >> 7) & 0x01) != 0,
            stage8: ((reg >> 8) & 0x01) != 0,
            stage9: ((reg >> 9) & 0x01) != 0,
            stage10: ((reg >> 10) & 0x01) != 0,
            stage11: ((reg >> 11) & 0x01) != 0,
        }
    }
}
